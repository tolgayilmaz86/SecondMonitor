﻿using System.Collections.ObjectModel;

namespace SecondMonitor.Timing.CarSettings
{
    using System.Windows.Input;

    using DataModel.BasicProperties;
    using SimdataManagement.ViewModel;
    using ViewModels;
    using ViewModels.Factory;
    using ViewModels.Settings;
    using ViewModels.Settings.ViewModel;

    public class CarSettingsWindowViewModel : AbstractViewModel
    {
        private readonly DisplaySettingsViewModel _displaySettingsViewModel;

        private ICommand _cancelButtonCommand;
        private ICommand _okButtonCommand;
        private ICommand _copyCompoundToLocalCommand;
        private ObservableCollection<TyreCompoundPropertiesViewModel> _tyreSettingsViewModels;
        private TyreCompoundPropertiesViewModel _selectedTyreSettingsViewModel;


        private CarModelPropertiesViewModel _carModelPropertiesViewModel;
        private ICommand _openTyreDiameterWizardCommand;

        public CarSettingsWindowViewModel(IViewModelFactory viewModelFactory, ISettingsProvider settingsProvider)
        {
            _displaySettingsViewModel = settingsProvider.DisplaySettingsViewModel;
            LastCarDataViewMode = viewModelFactory.Create<LastCarDataViewModel>();
        }

        public LastCarDataViewModel LastCarDataViewMode { get; }

        public PressureUnits PressureUnits => _displaySettingsViewModel.PressureUnits;

        public TemperatureUnits TemperatureUnits => _displaySettingsViewModel.TemperatureUnits;

        public ObservableCollection<TyreCompoundPropertiesViewModel> TyreSettingsViewModels
        {
            get => _tyreSettingsViewModels;
            set
            {
                _tyreSettingsViewModels = value;
                NotifyPropertyChanged();
            }
        }

        public bool WheelRotationVisible { get; }

        public TyreCompoundPropertiesViewModel SelectedTyreSettingsViewModel
        {
            get => _selectedTyreSettingsViewModel;
            set
            {
                _selectedTyreSettingsViewModel = value;
                NotifyPropertyChanged();
            }
        }

        public CarModelPropertiesViewModel CarModelPropertiesViewModel
        {
            get => _carModelPropertiesViewModel;
            set
            {
                _carModelPropertiesViewModel = value;
                NotifyPropertyChanged();
            }
        }

        public ICommand CancelButtonCommand
        {
            get => _cancelButtonCommand;
            set
            {
                _cancelButtonCommand = value;
                NotifyPropertyChanged();
            }
        }

        public ICommand OkButtonCommand
        {
            get => _okButtonCommand;
            set
            {
                _okButtonCommand = value;
                NotifyPropertyChanged();
            }
        }

        public ICommand CopyCompoundToLocalCommand
        {
            get => _copyCompoundToLocalCommand;
            set
            {
                _copyCompoundToLocalCommand = value;
                NotifyPropertyChanged();
            }
        }

        public DistanceUnits DistanceUnits => _displaySettingsViewModel.DistanceUnitsVerySmall;

        public ICommand OpenTyreDiameterWizardCommand
        {
            get => _openTyreDiameterWizardCommand;
            set => SetProperty(ref _openTyreDiameterWizardCommand, value);
        }
    }
}