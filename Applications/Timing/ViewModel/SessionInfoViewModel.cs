﻿namespace SecondMonitor.Timing.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using DataModel.BasicProperties;
    using DataModel.Extensions;
    using DataModel.Snapshot;
    using SessionTiming.Drivers.ViewModel;
    using SessionTiming.ViewModel;
    using ViewModels;
    using ViewModels.CarStatus;

    public class SessionInfoViewModel : AbstractViewModel, ISimulatorDataSetViewModel, IPaceProvider
    {
        public const string ViewModelLayoutName = "Session Best and Time Remaining";

        private static readonly TimeSpan SessionRemainingFrequency = TimeSpan.FromSeconds(1);

        private string _bestSector1;
        private string _bestSector2;
        private string _bestSector3;
        private bool _anySectorFilled;
        private string _bestLap;
        private string _sessionRemaining;
        private readonly Stopwatch _refreshWatch;

        private SessionTiming _timing;
        private readonly SessionRemainingCalculator _sessionRemainingCalculator;

        public SessionInfoViewModel()
        {
            _timing = null;
            _refreshWatch = Stopwatch.StartNew();
            BestSector1 = "S1:N/A";
            BestSector2 = "S2:N/A";
            BestSector3 = "S2:N/A";
            BestLap = "Best Lap: N/A";
            SessionRemaining = "L1/14";
            AnySectorFilled = true;
            _sessionRemainingCalculator = new SessionRemainingCalculator(this);
        }

        public TimeSpan? PlayersPace => _timing?.Player?.Pace;

        public TimeSpan? LeadersPace => _timing?.Leader?.Pace;

        public Dictionary<string, TimeSpan> GetPaceForDriversMap()
        {
            if (_timing == null)
            {
                return new Dictionary<string, TimeSpan>();
            }

            return _timing.Drivers.ToDictionary(x => x.Value.DriverId, x => x.Value.Pace);
        }


        public string BestSector1
        {
            get => _bestSector1;
            set => SetProperty(ref _bestSector1, value);
        }

        public string BestSector2
        {
            get => _bestSector2;
            set => SetProperty(ref _bestSector2, value);
        }

        public string BestSector3
        {
            get => _bestSector3;
            set => SetProperty(ref _bestSector3, value);
        }

        public string BestLap
        {
            get => _bestLap;
            set => SetProperty(ref _bestLap, value);
        }

        public string SessionRemaining
        {
            get => _sessionRemaining;
            set => SetProperty(ref _sessionRemaining, value);
        }

        public bool AnySectorFilled
        {
            get => _anySectorFilled;
            set => SetProperty(ref _anySectorFilled, value);
        }

        private void RefreshAll()
        {
            if (SessionTiming == null)
            {
                return;
            }
            BestSector1 = FormatSectorTime(SessionTiming.SessionBestTimesViewModel.BestSector1);
            BestSector2 = FormatSectorTime(SessionTiming.SessionBestTimesViewModel.BestSector2);
            BestSector3 = FormatSectorTime(SessionTiming.SessionBestTimesViewModel.BestSector3);
            BestLap = FormatBestLap(SessionTiming.SessionBestTimesViewModel.BestLap);
        }

        private void UpdateAnySectorsFilled()
        {
            AnySectorFilled = !(string.IsNullOrEmpty(BestSector1) && string.IsNullOrEmpty(BestSector2)
                                     && string.IsNullOrEmpty(BestSector3));
        }

        public SessionTiming SessionTiming
        {
            get => _timing;

            set
            {
                if (SessionTiming != null)
                {
                    SessionTiming.SessionBestTimesViewModel.PropertyChanged -= SessionTimingOnSessionBestChanged;
                }
                _timing = value;
                if (SessionTiming != null)
                {
                    SessionTiming.SessionBestTimesViewModel.PropertyChanged += SessionTimingOnSessionBestChanged;
                }
                RefreshAll();
                UpdateAnySectorsFilled();
            }
        }

        private void SessionTimingOnSessionBestChanged(object sender, EventArgs eventArgs)
        {
            BestSector1 = FormatSectorTime(SessionTiming.SessionBestTimesViewModel.BestSector1);
            BestSector2 = FormatSectorTime(SessionTiming.SessionBestTimesViewModel.BestSector2);
            BestSector3 = FormatSectorTime(SessionTiming.SessionBestTimesViewModel.BestSector3);
            BestLap = FormatBestLap(SessionTiming.SessionBestTimesViewModel.BestLap);
            UpdateAnySectorsFilled();
        }

        private string FormatSectorTime(SectorTiming sectorTiming)
        {
            if (sectorTiming == null)
            {
                return string.Empty;
            }
            return "S" + sectorTiming.SectorNumber + ":" + sectorTiming.Lap.Driver.DriverInfo.DriverShortName + "-(L" + sectorTiming.Lap.LapNumber + "):"
                   + TimeSpanFormatHelper.FormatTimeSpanOnlySeconds(sectorTiming.Duration, false);
        }

        private string FormatBestLap(ILapInfo bestLap)
        {
            if (bestLap == null)
            {
                return "No Best Lap";
            }

            return bestLap.Driver.DriverInfo.DriverShortName + "-(L" + bestLap.LapNumber + "):"
                   + bestLap.LapTime.FormatToDefault();
        }

        private string GetSessionRemaining(SimulatorDataSet dataSet)
        {
            if (dataSet.SessionInfo.SessionLengthType == SessionLengthType.Na)
            {
                return "NA";
            }

            if (dataSet.SessionInfo.SessionLengthType == SessionLengthType.Time || dataSet.SessionInfo.SessionLengthType == SessionLengthType.TimeWithExtraLap)
            {
                string prefix = dataSet.SessionInfo.SessionLengthType == SessionLengthType.Time ? "Time: " : "Time (+1 Lap): ";
                string timeRemaining =  prefix + _sessionRemainingCalculator.GetTimeRemaining(dataSet).FormatToHoursMinutesSeconds();
                if (_timing?.Leader != null && dataSet.SessionInfo?.SessionType == SessionType.Race && _timing?.Leader?.Pace != TimeSpan.Zero)
                {
                    double lapsRemaining = _sessionRemainingCalculator.GetLapsRemaining(dataSet);
                    double totalLaps = Math.Ceiling(lapsRemaining + dataSet.PlayerInfo.CompletedLaps);
                    timeRemaining += $"\nEst. Laps: {(Math.Floor(lapsRemaining * 10) / 10.0):N1} / {totalLaps:N0}";
                }

                return timeRemaining;
            }

            if (dataSet.SessionInfo.SessionLengthType == SessionLengthType.Laps)
            {
                int lapsToGo = dataSet.SessionInfo.TotalNumberOfLaps - dataSet.SessionInfo.LeaderCurrentLap + 1;
                if (lapsToGo < 1)
                {
                    return "Leader Finished";
                }
                if (lapsToGo == 1)
                {
                    return "Leader on Final Lap";
                }
                string lapsToDisplay = lapsToGo < 2000
                                           ? lapsToGo.ToString()
                                           : "Infinite";
                if (_timing?.Leader != null && dataSet.SessionInfo?.SessionType == SessionType.Race && _timing?.Leader?.Pace != TimeSpan.Zero)
                {
                    lapsToDisplay += "\nEst. Time: " + _sessionRemainingCalculator.GetTimeRemaining(dataSet).FormatToHoursMinutesSeconds();
                }
                return "Leader laps to go: " + lapsToDisplay;
            }

            return "NA";
        }

        public void ApplyDateSet(SimulatorDataSet dataSet)
        {
            if (_refreshWatch.Elapsed < SessionRemainingFrequency)
            {
                return;
            }

            SessionRemaining = GetSessionRemaining(dataSet);
           _refreshWatch.Restart();
        }

        public void Reset()
        {
            _refreshWatch.Restart();
            _sessionRemainingCalculator.Reset();
        }
    }
}