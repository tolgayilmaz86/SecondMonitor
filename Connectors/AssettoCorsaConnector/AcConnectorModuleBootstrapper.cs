﻿namespace SecondMonitor.AssettoCorsaConnector
{
    using System.Collections.Generic;
    using System.Linq;
    using Contracts.NInject;
    using Ninject.Modules;

    public class AcConnectorModuleBootstrapper : INinjectModuleBootstrapper
    {
        public IList<INinjectModule> GetModules()
        {
            return new INinjectModule[] {new AcConnectorModule(), }.ToList();
        }
    }
}