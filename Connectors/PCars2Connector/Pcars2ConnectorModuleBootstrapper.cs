﻿namespace SecondMonitor.PCars2Connector
{
    using System.Collections.Generic;
    using System.Linq;
    using Contracts.NInject;
    using Ninject.Modules;

    public class Pcars2ConnectorModuleBootstrapper : INinjectModuleBootstrapper
    {
        public IList<INinjectModule> GetModules()
        {
            return new INinjectModule[] {new PCars2ConnectorModule(), }.ToList();
        }
    }
}