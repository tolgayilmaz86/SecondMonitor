﻿namespace SecondMonitor.Contracts.FuelInformation
{
    using DataModel.BasicProperties;
    using DataModel.Summary;

    public interface ISessionFuelConsumptionViewModel
    {
         string TrackName { get; set; }
         Distance LapDistance { get; set; }
         string SessionType { get; set; }
         FuelConsumptionInfo FuelConsumption { get; set; }
         Volume AvgPerMinute { get; set; }
         Volume AvgPerLap { get; set; }

    }
}