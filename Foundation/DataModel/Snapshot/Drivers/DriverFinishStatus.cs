﻿namespace SecondMonitor.DataModel.Snapshot.Drivers
{
    public enum DriverFinishStatus
    {
        Na, None, Finished, Dnf, Dnq, Dns, Dq
    }

    public static class DriverFinishStatusExtension
    {
        public static bool IsFinishedRaceStatus(this DriverFinishStatus driverFinishStatus)
        {
            return driverFinishStatus == DriverFinishStatus.Dnf || driverFinishStatus == DriverFinishStatus.Dq || driverFinishStatus == DriverFinishStatus.Finished;
        }
    }
}