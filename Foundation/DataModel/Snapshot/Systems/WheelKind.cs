﻿namespace SecondMonitor.DataModel.Snapshot.Systems
{
    public enum WheelKind
    {
        FrontLeft, FrontRight, RearLeft, RearRight
    }
}