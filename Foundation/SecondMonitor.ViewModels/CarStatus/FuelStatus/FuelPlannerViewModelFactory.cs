﻿using System.Linq;
using SecondMonitor.DataModel.Extensions;

namespace SecondMonitor.ViewModels.CarStatus.FuelStatus
{
    using System;
    using System.Collections.Generic;
    using DataModel.BasicProperties;
    using DataModel.Summary.FuelConsumption;

    public class FuelPlannerViewModelFactory
    {
        public FuelPlannerViewModel Create(IReadOnlyCollection<SessionFuelConsumptionDto> consumptionEntries)
        {

            FuelPlannerViewModel newViewModel =  new FuelPlannerViewModel();

            if (consumptionEntries.Count == 0)
            {
                return newViewModel;
            }

            List<SessionFuelConsumptionDto> drySessions = consumptionEntries.Where(x => !x.IsWetSession).ToList();
            if (drySessions.Count != 0)
            {
                SessionFuelConsumptionViewModel averageConsumption = new SessionFuelConsumptionViewModel();
                averageConsumption.FromModel(CreateAggregated(consumptionEntries));
                averageConsumption.SessionType = "Total (Dry)";
                newViewModel.Sessions.Add(averageConsumption);
            }

            consumptionEntries.ForEach(x =>
            {
                SessionFuelConsumptionViewModel consumptionViewModel = new SessionFuelConsumptionViewModel();
                if (x == null)
                {
                    return;
                }

                consumptionViewModel.FromModel(x);
                newViewModel.Sessions.Add(consumptionViewModel);
            });

            newViewModel.SelectedSession = newViewModel.Sessions.First();
            return newViewModel;
        }

        private SessionFuelConsumptionDto CreateAggregated(IReadOnlyCollection<SessionFuelConsumptionDto> consumptionEntries)
        {
            SessionFuelConsumptionDto baseConsumption = consumptionEntries.First();
            return new SessionFuelConsumptionDto(baseConsumption.Simulator, baseConsumption.TrackFullName, baseConsumption.LapDistance, baseConsumption.CarName,
                baseConsumption.SessionKind, consumptionEntries.Sum(x => x.ElapsedSeconds), consumptionEntries.Sum(x => x.TraveledDistanceMeters), Volume.FromLiters(consumptionEntries.Select(x => x.ConsumedFuel.InLiters).Sum(x => x)),
                DateTime.Now, baseConsumption.IsWetSession);
        }
    }
}