﻿namespace SecondMonitor.ViewModels.CarStatus
{
    using System;
    using System.ComponentModel;
    using DataModel.BasicProperties;
    using DataModel.Snapshot;
    using DataModel.Snapshot.Systems;
    using FuelStatus;
    using Settings;

    public class WheelStatusViewModel : AbstractViewModel
    {
        private readonly ISettingsProvider _settingsProvider;
        private readonly TyreLifeTimeMonitor _tyreLifeTimeMonitor;

        public WheelStatusViewModel(bool isLeft, ISettingsProvider settingsProvider)
        {
            _settingsProvider = settingsProvider;
            IsLeftWheel = isLeft;
        }

        public WheelStatusViewModel(bool isLeft, SessionRemainingCalculator sessionRemainingCalculator, ISettingsProvider settingsProvider, IPaceProvider paceProvider, IFuelPredictionProvider fuelPredictionProvider) : this(isLeft, settingsProvider)
        {
            _tyreLifeTimeMonitor = new TyreLifeTimeMonitor(paceProvider, sessionRemainingCalculator, fuelPredictionProvider);
            ApplyDisplaySettings();
            settingsProvider.DisplaySettingsViewModel.PropertyChanged+=DisplaySettingsViewModelOnPropertyChanged;
        }

        private double _wheelCamber;
        public double WheelCamber
        {
            get => _wheelCamber;
            set => SetProperty(ref _wheelCamber, value);
        }


        private double _brakeCondition;
        public double BrakeCondition
        {
            get => _brakeCondition;
            set => SetProperty(ref _brakeCondition, value);
        }

        private int _lapsUntilHeavyWear;
        public int LapsUntilHeavyWear
        {
            get => _lapsUntilHeavyWear;
            set => SetProperty(ref _lapsUntilHeavyWear, value);
        }

        private string _tyreCompound;
        public string TyreCompound
        {
            get => _tyreCompound;
            set => SetProperty(ref _tyreCompound, value);
        }

        private bool _isSliding;

        public bool IsSliding
        {
            get => _isSliding;
            set => SetProperty(ref _isSliding, value);
        }

        private bool _isTyreDetached;
        public bool IsTyreDetached
        {
            get => _isTyreDetached;
            set => SetProperty(ref _isTyreDetached, value);
        }

        private TemperatureUnits _temperatureUnits;
        public TemperatureUnits TemperatureUnits
        {
            get => _temperatureUnits;
            set => SetProperty(ref _temperatureUnits, value);
        }

        private PressureUnits _pressureUnits;
        public PressureUnits PressureUnits
        {
            get => _pressureUnits;
            set => SetProperty(ref _pressureUnits, value);
        }

        private double _tyreCondition;
        public double TyreCondition
        {
            get => _tyreCondition;
            set => SetProperty(ref _tyreCondition, value);
        }

        private double _tyreNoWearWearLimit;
        public double TyreNoWearWearLimit
        {
            get => _tyreNoWearWearLimit;
            set => SetProperty(ref _tyreNoWearWearLimit, value);
        }

        private double _tyreMildWearLimit;
        public double TyreMildWearLimit
        {
            get => _tyreMildWearLimit;
            set => SetProperty(ref _tyreMildWearLimit, value);
        }

        private double _tyreHeavyWearLimit;
        public double TyreHeavyWearLimit
        {
            get => _tyreHeavyWearLimit;
            set => SetProperty(ref _tyreHeavyWearLimit, value);
        }

        private OptimalQuantity<Temperature> _tyreCoreTemperature;
        public OptimalQuantity<Temperature> TyreCoreTemperature
        {
            get => _tyreCoreTemperature;
            set => SetProperty(ref _tyreCoreTemperature, value);
        }

        private OptimalQuantity<Temperature> _tyreLeftTemperature;
        public OptimalQuantity<Temperature> TyreLeftTemperature
        {
            get => _tyreLeftTemperature;
            set => SetProperty(ref _tyreLeftTemperature, value);
        }

        private OptimalQuantity<Temperature> _tyreCenterTemperature;
        public OptimalQuantity<Temperature> TyreCenterTemperature
        {
            get => _tyreCenterTemperature;
            set => SetProperty(ref _tyreCenterTemperature, value);
        }

        private OptimalQuantity<Temperature> _tyreRightTemperature;
        public OptimalQuantity<Temperature> TyreRightTemperature
        {
            get => _tyreRightTemperature;
            set => SetProperty(ref _tyreRightTemperature, value);
        }

        private OptimalQuantity<Temperature> _brakeTemperature;
        public OptimalQuantity<Temperature> BrakeTemperature
        {
            get => _brakeTemperature;
            set => SetProperty(ref _brakeTemperature, value);
        }

        private OptimalQuantity<Pressure> _tyrePressure;
        public OptimalQuantity<Pressure> TyrePressure
        {
            get => _tyrePressure;
            set => SetProperty(ref _tyrePressure, value);
        }

        private bool _isLeftWheel;
        public bool IsLeftWheel
        {
            get => _isLeftWheel;
            set => SetProperty(ref _isLeftWheel, value);
        }

        private double _wearAtRaceEnd;

        public double WearAtRaceEnd
        {
            get => _wearAtRaceEnd;
            set => SetProperty(ref _wearAtRaceEnd, value);
        }

        private double _wearAtStintEnd;
        public double WearAtStintEnd
        {
            get => _wearAtStintEnd;
            set => SetProperty(ref _wearAtStintEnd, value);
        }

        public void ApplyWheelCondition(WheelInfo wheelInfo)
        {

            TyreCondition = wheelInfo.Detached ? 0.1 : Math.Round(100 * (1 -wheelInfo.TyreWear.ActualWear), 1);
            TyreNoWearWearLimit = 100 * (1 - wheelInfo.TyreWear.NoWearWearLimit);
            TyreMildWearLimit = 100 * (1 - wheelInfo.TyreWear.LightWearLimit);
            TyreHeavyWearLimit = 100 * (1 - wheelInfo.TyreWear.HeavyWearLimit);

            if (wheelInfo.TyreCoreTemperature.ActualQuantity.InCelsius > -200 && (TyreCoreTemperature == null || Math.Abs(TyreCoreTemperature.ActualQuantity.RawValue - wheelInfo.TyreCoreTemperature.ActualQuantity.RawValue) > 0.5))
            {
                TyreCoreTemperature = wheelInfo.TyreCoreTemperature;
            }

            if (TyreLeftTemperature == null || Math.Abs(TyreLeftTemperature.ActualQuantity.RawValue - wheelInfo.LeftTyreTemp.ActualQuantity.RawValue) > 0.5)
            {
                TyreLeftTemperature = wheelInfo.LeftTyreTemp;
            }

            if (wheelInfo.CenterTyreTemp.ActualQuantity.InCelsius > -200 && (TyreCenterTemperature == null || Math.Abs(TyreCenterTemperature.ActualQuantity.RawValue - wheelInfo.CenterTyreTemp.ActualQuantity.RawValue) > 0.5))
            {
                TyreCenterTemperature = wheelInfo.CenterTyreTemp;
            }

            if (TyreRightTemperature == null || Math.Abs(TyreRightTemperature.ActualQuantity.RawValue - wheelInfo.RightTyreTemp.ActualQuantity.RawValue) > 0.5)
            {

                TyreRightTemperature = wheelInfo.RightTyreTemp;
            }

            if (TyrePressure == null || Math.Abs(TyrePressure.ActualQuantity.RawValue - wheelInfo.TyrePressure.ActualQuantity.RawValue) > 0.5)
            {
                TyrePressure = wheelInfo.TyrePressure;
            }

            if (BrakeTemperature == null || Math.Abs(BrakeTemperature.ActualQuantity.RawValue - wheelInfo.BrakeTemperature.ActualQuantity.RawValue) > 0.5)
            {
                BrakeTemperature = wheelInfo.BrakeTemperature;
            }

            UpdateCompound(wheelInfo);
            IsTyreDetached = wheelInfo.Detached;
            BrakeCondition = Math.Round(100 * (1 - wheelInfo.BrakesDamage.Damage), 0);

            if (_settingsProvider.DisplaySettingsViewModel.EnableCamberVisualization)
            {
                WheelCamber = Math.Round(wheelInfo.Camber.GetValueInUnits(AngleUnits.Degrees), 1);
            }
            else
            {
                WheelCamber = 0;
            }
        }

        private void UpdateCompound(WheelInfo wheelInfo)
        {
            TyreCompound = wheelInfo.TyreType;
        }

        public void UpdateSlippingInformation(SimulatorDataSet dataSet, WheelInfo wheelInfo, double radiusInMeters)
        {
            if (dataSet?.PlayerInfo == null || wheelInfo == null)
            {
                IsSliding = false;
                return;
            }

            if (!dataSet.SimulatorSourceInfo.TelemetryInfo.ContainsWheelRps || dataSet.PlayerInfo.Speed.InKph < 30)
            {
                IsSliding = false;
                return;
            }

            double wheelVelocity = wheelInfo.Rps * radiusInMeters;
            IsSliding = wheelVelocity < dataSet.PlayerInfo.Speed.InMs * 0.7;
        }

        public void ApplyWheelCondition(SimulatorDataSet dataSet, WheelInfo wheelInfo)
        {
            if (dataSet.PlayerInfo == null)
            {
                return;
            }

            if (dataSet.SessionInfo.SessionType != SessionType.Race && dataSet.PlayerInfo.InPits)
            {
                UpdateCompound(wheelInfo);
                return;
            }

            ApplyWheelCondition(wheelInfo);

            if (_tyreLifeTimeMonitor == null)
            {
                return;
            }

            _tyreLifeTimeMonitor.ApplyWheelInfo(dataSet, wheelInfo);
            WearAtRaceEnd = Math.Round(_tyreLifeTimeMonitor.WearAtRaceEnd);
            WearAtStintEnd = Math.Round(_tyreLifeTimeMonitor.WearAtStintEnd);
            LapsUntilHeavyWear = _tyreLifeTimeMonitor.LapsUntilHeavyWear;
        }

        public void Reset()
        {
            _tyreLifeTimeMonitor?.Reset();
        }

        private void DisplaySettingsViewModelOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            ApplyDisplaySettings();
        }

        private void ApplyDisplaySettings()
        {
            PressureUnits = _settingsProvider.DisplaySettingsViewModel.PressureUnits;
            TemperatureUnits = _settingsProvider.DisplaySettingsViewModel.TemperatureUnits;
        }
    }
}