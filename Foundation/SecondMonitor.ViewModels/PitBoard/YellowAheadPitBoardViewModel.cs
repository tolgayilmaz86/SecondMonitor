﻿namespace SecondMonitor.ViewModels.PitBoard
{
    public class YellowAheadPitBoardViewModel : AbstractViewModel
    {
        private string _driverName;
        private string _distance;

        public YellowAheadPitBoardViewModel()
        {
            DriverName = "Fedor Predkozisko";
            Distance = "200 m";
        }

        public string DriverName
        {
            get => _driverName;
            set => SetProperty(ref _driverName, value);
        }

        public string Distance
        {
            get => _distance;
            set => SetProperty(ref _distance, value);
        }
    }
}