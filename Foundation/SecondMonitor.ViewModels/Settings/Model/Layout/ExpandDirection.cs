﻿namespace SecondMonitor.ViewModels.Settings.Model.Layout
{
    public enum ExpandDirection
    {
       Up, Down, Left, Right
    }
}