﻿namespace SecondMonitor.ViewModels.Settings.Model
{
    using Layout;
    using Layouts;

    public class LayoutSettings
    {
        public LayoutDescription Description { get; set; }
    }
}