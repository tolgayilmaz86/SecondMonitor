﻿namespace SecondMonitor.ViewModels.Track
{
    using Contracts.Session;
    using DataModel.BasicProperties;
    using DataModel.Snapshot;
    using DataModel.Snapshot.Drivers;

    public class DriverPositionViewModel : AbstractViewModel
    {
        private double _x;
        private double _y;
        private double _lapCompletedPercentage;
        private int _position;
        private bool _isPlayer;
        private ColorDto _classIndicationColor;
        private ColorDto _outLineColor;
        private DriverState _driverState;

        public DriverPositionViewModel(string driverLongName, string driverId)
        {
            DriverLongName = driverLongName;
            DriverId = driverId;
        }

        public string DriverLongName { get; }
        public string DriverId { get; }

        public double X
        {
            get => _x;
            set => SetProperty(ref _x, value);
        }

        public double Y
        {
            get => _y;
            set => SetProperty(ref _y, value);
        }

        public int Position
        {
            get => _position;
            set => SetProperty(ref _position, value);
        }

        public bool IsPlayer
        {
            get => _isPlayer;
            set => SetProperty(ref _isPlayer, value);
        }

        public ColorDto ClassIndicationColor
        {
            get => _classIndicationColor;
            set => SetProperty(ref _classIndicationColor, value);
        }

        public ColorDto OutLineColor
        {
            get => _outLineColor;
            set => SetProperty(ref _outLineColor, value);
        }

        public double LapCompletedPercentage
        {
            get => _lapCompletedPercentage;
            set => SetProperty(ref _lapCompletedPercentage, value);
        }

        public DriverState DriverState
        {
            get => _driverState;
            set => SetProperty(ref _driverState, value);
        }

        public void UpdateStatus(SimulatorDataSet dataSet, IDriverInfo driverInfo, ISessionInformationProvider sessionInformationProvider)
        {
            if (driverInfo.FinishStatus == DriverFinishStatus.Finished)
            {
                DriverState = DriverState.Finished;
                return;
            }

            if (driverInfo.InPits)
            {
                DriverState = driverInfo.Speed.InKph <= 10 ? DriverState.InPits : DriverState.InPitsMoving;
                return;
            }

            if (driverInfo.IsCausingYellow)
            {
                DriverState = DriverState.Yellow;
                return;
            }

            if (driverInfo.IsBeingLappedByPlayer || (dataSet.SessionInfo.SessionType != SessionType.Race && !sessionInformationProvider.IsDriverOnValidLap(driverInfo)))
            {
                DriverState = DriverState.Lapped;
                return;
            }

            if (driverInfo.IsLappingPlayer)
            {
                DriverState = DriverState.Lapping;
                return;
            }

            DriverState = DriverState.Normal;
        }
    }
}