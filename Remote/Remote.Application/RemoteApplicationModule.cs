﻿namespace SecondMonitor.Remote.Application
{
    using Common.Adapter;
    using Controllers;
    using Ninject.Modules;
    using ViewModels;

    public class RemoteApplicationModule : NinjectModule
    {
        public override void Load()
        {
            Rebind<IBroadCastServerController>().To<BroadCastServerController>();
            Rebind<IServerOverviewViewModel>().To<ServerOverviewViewModel>().InSingletonScope();
            Rebind<IClientViewModel>().To<ClientViewModel>();
            Rebind<IDatagramPayloadPacker>().To<DatagramPayloadPacker>();
        }
    }
}