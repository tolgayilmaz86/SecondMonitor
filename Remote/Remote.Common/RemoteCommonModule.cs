﻿namespace SecondMonitor.Remote.Common
{
    using Adapter;
    using Comparators;
    using Ninject.Modules;

    public class RemoteCommonModule : NinjectModule
    {
        public override void Load()
        {
            Rebind<IDatagramPayloadUnPacker>().To<DatagramPayloadUnPacker>();
            Rebind<IDatagramPayloadPacker>().To<DatagramPayloadPacker>();
            Rebind<ISimulatorSourceInfoComparator>().To<SimulatorSourceInfoComparator>();
        }
    }
}