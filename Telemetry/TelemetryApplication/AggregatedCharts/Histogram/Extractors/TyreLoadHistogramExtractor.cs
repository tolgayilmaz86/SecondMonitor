﻿namespace SecondMonitor.Telemetry.TelemetryApplication.AggregatedCharts.Histogram.Extractors
{
    using System;
    using DataAdapters;
    using DataModel.BasicProperties;
    using DataModel.Snapshot;
    using DataModel.Snapshot.Systems;
    using SecondMonitor.ViewModels.Settings;

    public class TyreLoadHistogramExtractor : AbstractWheelHistogramDataExtractor
    {
        private readonly TyreLoadAdapter _tyreLoadAdapter;

        public TyreLoadHistogramExtractor(ISettingsProvider settingsProvider, TyreLoadAdapter tyreLoadAdapter) : base(settingsProvider)
        {
            _tyreLoadAdapter = tyreLoadAdapter;
        }

        protected override bool ZeroBandInMiddle => true;
        public override string YUnit => Force.GetUnitSymbol(ForceUnits);
        public override double DefaultBandSize => Math.Round(Force.GetFromNewtons(100).GetValueInUnits(ForceUnits));
        protected override Func<SimulatorSourceInfo, WheelInfo, double> WheelValueExtractor => GetTyreLoad;

        private double GetTyreLoad(SimulatorSourceInfo simulatorSourceInfo, WheelInfo wheelInfo)
        {
            return _tyreLoadAdapter.GetQuantityFromWheel(simulatorSourceInfo, wheelInfo).GetValueInUnits(ForceUnits);
        }
    }
}