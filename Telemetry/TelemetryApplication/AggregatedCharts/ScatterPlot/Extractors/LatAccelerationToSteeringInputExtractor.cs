﻿namespace SecondMonitor.Telemetry.TelemetryApplication.AggregatedCharts.ScatterPlot.Extractors
{
    using System.Collections.Generic;
    using DataModel.BasicProperties;
    using DataModel.Telemetry;
    using Filter;
    using SecondMonitor.ViewModels.Settings;

    public class LatAccelerationToSteeringInputExtractor : AbstractScatterPlotExtractor
    {
        private readonly SpeedFilter _speedFilter;

        public LatAccelerationToSteeringInputExtractor(ISettingsProvider settingsProvider, SpeedFilter speedFilter) : base(settingsProvider, new List<ITelemetryFilter>(){speedFilter})
        {
            _speedFilter = speedFilter;
        }

        public override string YUnit => "%";
        public override string XUnit => "G";
        public override double XMajorTickSize => 1;
        public override double YMajorTickSize => 50;

        public Velocity MinimalSpeed
        {
            get => _speedFilter.MinimalSpeed;
            set => _speedFilter.MinimalSpeed = value;
        }

        public Velocity MaximumSpeed
        {
            get => _speedFilter.MaximumSpeed;
            set => _speedFilter.MaximumSpeed = value;
        }

        protected override double GetXValue(TimedTelemetrySnapshot snapshot)
        {
            return snapshot.PlayerData.CarInfo.Acceleration.XinG;
        }

        protected override double GetYValue(TimedTelemetrySnapshot snapshot)
        {
            return snapshot.InputInfo.SteeringInput * 100;
        }
    }
}