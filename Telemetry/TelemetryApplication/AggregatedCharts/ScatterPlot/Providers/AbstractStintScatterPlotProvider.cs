﻿namespace SecondMonitor.Telemetry.TelemetryApplication.AggregatedCharts.ScatterPlot.Providers
{
    using Controllers.Synchronization;
    using Extractors;
    using ViewModels.AggregatedCharts.ScatterPlot;
    using ViewModels.LoadedLapCache;

    public abstract class AbstractStintScatterPlotProvider : GenericStintScatterPlotProvider<ScatterPlot, ScatterPlotChartViewModel>
    {
        protected AbstractStintScatterPlotProvider(ILoadedLapsCache loadedLapsCache, AbstractScatterPlotExtractor dataExtractor, IDataPointSelectionSynchronization dataPointSelectionSynchronization) : base(loadedLapsCache, dataExtractor,
            dataPointSelectionSynchronization)
        {
        }

        protected override void OnNewViewModel(ScatterPlot scatterPlot, ScatterPlotChartViewModel scatterPlotChartViewModel)
        {
        }
    }
}