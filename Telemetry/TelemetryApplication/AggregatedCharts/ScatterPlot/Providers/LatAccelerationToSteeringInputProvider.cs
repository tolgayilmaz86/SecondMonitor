﻿namespace SecondMonitor.Telemetry.TelemetryApplication.AggregatedCharts.ScatterPlot.Providers
{
    using Controllers.Settings;
    using Controllers.Settings.Car;
    using Controllers.Synchronization;
    using DataModel.BasicProperties;
    using Extractors;
    using ViewModels.AggregatedCharts.ScatterPlot;
    using ViewModels.LoadedLapCache;

    public class LatAccelerationToSteeringInputProvider : GenericStintScatterPlotProvider<ScatterPlot, FilteredScatterPlotChartViewModel>
    {
        private readonly LatAccelerationToSteeringInputExtractor _dataExtractor;
        private readonly ICarSettingsController _carSettingsController;

        public LatAccelerationToSteeringInputProvider(ILoadedLapsCache loadedLapsCache, LatAccelerationToSteeringInputExtractor dataExtractor, IDataPointSelectionSynchronization dataPointSelectionSynchronization, ISettingsController settingsController) : base(loadedLapsCache, dataExtractor, dataPointSelectionSynchronization)
        {
            _dataExtractor = dataExtractor;
            _carSettingsController = settingsController.CarSettingsController;
        }

        public override string ChartName => AggregatedChartNames.LatAccToSteeringInput;
        public override AggregatedChartKind Kind => AggregatedChartKind.ScatterPlot;

        protected override void OnNewViewModel(ScatterPlot scatterPlot, FilteredScatterPlotChartViewModel scatterPlotChartViewModel)
        {
            scatterPlotChartViewModel.QuantityName = "Speeds: ";
            scatterPlotChartViewModel.QuantityUnit = Velocity.GetUnitSymbol(_dataExtractor.VelocityUnits);
            scatterPlotChartViewModel.MinimalValue = _dataExtractor.MinimalSpeed.GetValueInUnits(_dataExtractor.VelocityUnits);
            scatterPlotChartViewModel.MaximumValue = _dataExtractor.MaximumSpeed.GetValueInUnits(_dataExtractor.VelocityUnits);
        }

        protected override void PreviewScatterPlotCreation()
        {
            base.PreviewScatterPlotCreation();
            if (_carSettingsController.CurrentCarProperties?.ChartsProperties?.LatAccToSteeringChartProperties == null)
            {
                return;
            }
            var chartProperties = _carSettingsController.CurrentCarProperties.ChartsProperties.LatAccToSteeringChartProperties;

            _dataExtractor.MinimalSpeed = chartProperties.MinimumSpeed;
            _dataExtractor.MaximumSpeed = chartProperties.MaximumSpeed;
        }

        protected override void PreviewRefreshViewModel(FilteredScatterPlotChartViewModel scatterPlotChartViewModel)
        {
            base.PreviewRefreshViewModel(scatterPlotChartViewModel);

            if (_carSettingsController.CurrentCarProperties?.ChartsProperties?.LatAccToSteeringChartProperties == null)
            {
                return;
            }

            var chartProperties = _carSettingsController.CurrentCarProperties.ChartsProperties.LatAccToSteeringChartProperties;

            chartProperties.MinimumSpeed = Velocity.FromUnits(scatterPlotChartViewModel.MinimalValue, _dataExtractor.VelocityUnits);
            chartProperties.MaximumSpeed = Velocity.FromUnits(scatterPlotChartViewModel.MaximumValue, _dataExtractor.VelocityUnits);
        }
    }
}