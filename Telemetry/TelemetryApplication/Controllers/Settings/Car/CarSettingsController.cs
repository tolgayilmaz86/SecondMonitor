﻿namespace SecondMonitor.Telemetry.TelemetryApplication.Controllers.Settings.Car
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows;
    using Contracts.Commands;
    using DefaultCarProperties;
    using SecondMonitor.ViewModels;
    using SecondMonitor.ViewModels.Factory;
    using Synchronization;
    using TelemetryApplication.Settings.DTO;
    using TelemetryApplication.Settings.DTO.CarProperties;
    using TelemetryApplication.Settings.DTO.ChartProperties;
    using ViewModels;

    public class CarSettingsController : ICarSettingsController
    {
        public event EventHandler<CarPropertiesArgs> CarPropertiesChanges;

        private readonly ITelemetryViewsSynchronization _telemetryViewsSynchronization;
        private readonly IWindowService _windowService;
        private readonly List<IDefaultCarPropertiesProvider> _defaultCarPropertiesProviders;
        private Window _settingsWindow;
        private readonly CarPropertiesViewModel _carPropertiesViewModel;

        public CarSettingsController(ITelemetryViewsSynchronization telemetryViewsSynchronization, IEnumerable<IDefaultCarPropertiesProvider> defaultCarPropertiesProviders,
            IMainWindowViewModel mainWindowViewModel, IWindowService windowService, IViewModelFactory viewModelFactory)
        {
            _telemetryViewsSynchronization = telemetryViewsSynchronization;
            _windowService = windowService;
            _defaultCarPropertiesProviders = defaultCarPropertiesProviders.ToList();
            mainWindowViewModel.LapSelectionViewModel.OpenCarSettingsCommand = new RelayCommand(OpenCarSettings);
            _carPropertiesViewModel = viewModelFactory.Create<CarPropertiesViewModel>();
            _carPropertiesViewModel.CancelCommand = new RelayCommand(CancelCarSettings);
            _carPropertiesViewModel.OkCommand = new RelayCommand(ConfirmCarSettings);
        }

        public ISettingsController ParentController { get; set; }

        public CarPropertiesDto CurrentCarProperties
        {
            get;
            private set;
        }

        public CarsProperties CarsProperties => ParentController.TelemetrySettings.CarsProperties;

        public Task StartControllerAsync()
        {
            _telemetryViewsSynchronization.NewSessionLoaded += TelemetryViewsSynchronizationOnNewSessionLoaded;
            return Task.CompletedTask;
        }

        public Task StopControllerAsync()
        {
            _telemetryViewsSynchronization.NewSessionLoaded -= TelemetryViewsSynchronizationOnNewSessionLoaded;
            return Task.CompletedTask;
        }

       public CarPropertiesDto ResetAndGetCarPropertiesForCurrentCar()
        {
            return ResetAndGetCarProperties(CurrentCarProperties.Simulator, CurrentCarProperties.CarName);
        }

        public CarPropertiesDto GetCarProperties(string simulator, string carName)
        {
            if (CarsProperties.TryGetCarProperties(simulator, carName, out CarPropertiesDto carProperties))
            {
                if (carProperties.ChartsProperties.CamberHistogram == null)
                {
                    carProperties.ChartsProperties.CamberHistogram = new CamberHistogram();
                }
                return carProperties;
            }

            carProperties = CreateDefaultCarProperties(simulator, carName);
            CarsProperties.SaveCarProperties(carProperties);
            return carProperties;
        }

        public CarPropertiesDto ResetAndGetCarProperties(string simulator, string carName)
        {
            var carProperties = CreateDefaultCarProperties(simulator, carName);
            CarsProperties.SaveCarProperties(carProperties);
            return carProperties;
        }

        public void UpdateCarProperties(CarPropertiesDto carPropertiesDto)
        {
            CarsProperties.Cars.RemoveAll(x => x.Simulator == carPropertiesDto.Simulator && x.CarName == carPropertiesDto.CarName);
            CarsProperties.Cars.Add(carPropertiesDto);
            if (CurrentCarProperties.Simulator == carPropertiesDto.Simulator && CurrentCarProperties.CarName == carPropertiesDto.CarName)
            {
                CurrentCarProperties = carPropertiesDto;
            }
            NotifyCarPropertiesChanged(carPropertiesDto);
        }

        private void NotifyCarPropertiesChanged(CarPropertiesDto carPropertiesDto)
        {
            CarPropertiesChanges?.Invoke(this, new CarPropertiesArgs(carPropertiesDto));
        }

        private CarPropertiesDto CreateDefaultCarProperties(string simulator, string carName)
        {
            CarPropertiesDto carPropertiesDto = null;
            return _defaultCarPropertiesProviders.Any(x => x.TryGetDefaultSettings(simulator, carName, out carPropertiesDto)) ? carPropertiesDto : new CarPropertiesDto()
            {
                CarName = carName,
                Simulator = simulator
            };
        }

        private void TelemetryViewsSynchronizationOnNewSessionLoaded(object sender, TelemetrySessionArgs e)
        {
            CurrentCarProperties = GetCarProperties(e.SessionInfoDto.Simulator, e.SessionInfoDto.CarName);
        }

        private void OpenCarSettings()
        {
            if (_settingsWindow != null)
            {
                _settingsWindow.Focus();
                return;
            }

            var currentCarProperties = CurrentCarProperties;
            _carPropertiesViewModel.FromModel(currentCarProperties);

            _settingsWindow = _windowService.OpenWindow(_carPropertiesViewModel, "Car Properties", WindowState.Normal, SizeToContent.WidthAndHeight, WindowStartupLocation.CenterOwner, OnSettingsWindowClose);
        }

        private void OnSettingsWindowClose()
        {
            _settingsWindow = null;
        }

        private void CancelCarSettings()
        {
            _settingsWindow?.Close();
        }

        private void ConfirmCarSettings()
        {
            var newCarSettings = _carPropertiesViewModel.SaveToNewModel();
            UpdateCarProperties(newCarSettings);
            _settingsWindow?.Close();
        }
    }
}