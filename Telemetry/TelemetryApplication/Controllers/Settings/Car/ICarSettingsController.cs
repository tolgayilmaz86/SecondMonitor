﻿namespace SecondMonitor.Telemetry.TelemetryApplication.Controllers.Settings.Car
{
    using System;
    using SecondMonitor.ViewModels.Controllers;
    using TelemetryApplication.Settings.DTO.CarProperties;

    public interface ICarSettingsController : IChildController<ISettingsController>
    {
        event EventHandler<CarPropertiesArgs> CarPropertiesChanges;

        CarPropertiesDto CurrentCarProperties { get; }

        CarPropertiesDto ResetAndGetCarPropertiesForCurrentCar();


        CarPropertiesDto GetCarProperties(string simulator, string carName);

        CarPropertiesDto ResetAndGetCarProperties(string simulator, string carName);

        void UpdateCarProperties(CarPropertiesDto carPropertiesDto);

    }
}