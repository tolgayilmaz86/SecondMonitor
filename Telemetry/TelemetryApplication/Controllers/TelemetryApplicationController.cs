﻿namespace SecondMonitor.Telemetry.TelemetryApplication.Controllers
{
    using System.Reflection;
    using System.Threading.Tasks;
    using System.Windows;
    using Contracts.NInject;
    using MainWindow;
    using SecondMonitor.ViewModels.Controllers;
    using Settings;
    using TelemetryManagement.DTO;
    using TelemetryManagement.Migrations;

    public class TelemetryApplicationController : IController
    {
        //This is just a dummy to load TelemetryManagement for Kernel bootstrap
        private readonly Window _mainWindow;

        private IMainWindowController _mainWindowController;

        public TelemetryApplicationController(Window mainWindow)
        {
            _mainWindow = mainWindow;
        }

        public async Task StartControllerAsync()
        {
            await StartChildControllers();
        }

        public async Task StopControllerAsync()
        {
            await _mainWindowController.StopControllerAsync();
        }

        public async Task OpenFromRepository(string sessionIdentifier)
        {
            await _mainWindowController.LoadTelemetrySession(sessionIdentifier);
        }

        public async Task OpenLastSessionFromRepository()
        {
            await _mainWindowController.LoadLastTelemetrySession();
        }

        private async Task StartChildControllers()
        {
            Assembly.Load("TelemetryManagement");
            _mainWindowController = new KernelWrapper().Get<IMainWindowController>();
            _mainWindowController.MainWindow = _mainWindow;
            await _mainWindowController.StartControllerAsync();
        }
    }
}