﻿namespace SecondMonitor.Telemetry.TelemetryApplication.DataAdapters
{
    using Controllers.Settings;
    using Controllers.Settings.Car;
    using DataModel.BasicProperties;
    using DataModel.Snapshot;
    using DataModel.Snapshot.Systems;
    using Settings.DTO.CarProperties;

    public class TyreLoadAdapter : IWheelQuantityAdapter<Force>
    {
        private readonly ICarSettingsController _carSettingsController;

        public TyreLoadAdapter(ISettingsController settingsController)
        {
            _carSettingsController = settingsController.CarSettingsController;
        }

        public bool IsQuantityComputed(SimulatorSourceInfo simulatorSource)
        {
            return !simulatorSource.TelemetryInfo.ContainsTyreLoad;
        }

        public Force GetQuantityFromWheel(SimulatorSourceInfo simulatorSource, WheelInfo wheelInfo)
        {
            return IsQuantityComputed(simulatorSource) ? ComputeTyreLoad(wheelInfo, _carSettingsController.CurrentCarProperties.GetPropertiesByWheelKind(wheelInfo.WheelKind)) : wheelInfo.TyreLoad;
        }

        private static Force ComputeTyreLoad(WheelInfo wheelInfo, WheelPropertiesDto wheelProperties)
        {
            return Force.GetFromNewtons((wheelInfo.SuspensionTravel.InMillimeter - wheelProperties.NeutralSuspensionTravel.InMillimeter) * wheelProperties.SprintStiffnessPerMm.InNewtons);
        }
    }
}