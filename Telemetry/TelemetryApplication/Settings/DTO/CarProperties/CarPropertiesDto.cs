﻿namespace SecondMonitor.Telemetry.TelemetryApplication.Settings.DTO.CarProperties
{
    using System;
    using System.Xml.Serialization;
    using ChartProperties;
    using DataModel.BasicProperties;
    using DataModel.Snapshot.Systems;

    public class CarPropertiesDto
    {
        public CarPropertiesDto()
        {
            FrontLeftTyre = new WheelPropertiesDto()
            {
                SprintStiffnessPerMm = Force.GetFromNewtons(330),
            };
            FrontRightTyre = new WheelPropertiesDto()
            {
                SprintStiffnessPerMm = Force.GetFromNewtons(330),
            };
            RearLeftTyre = new WheelPropertiesDto()
            {
                SprintStiffnessPerMm = Force.GetFromNewtons(225),
            };
            RearRightTyre = new WheelPropertiesDto()
            {
                SprintStiffnessPerMm = Force.GetFromNewtons(225),
            };
            SteeringLock = Angle.GetFromDegrees(15);
            ChartsProperties = new ChartsProperties();
        }

        [XmlAttribute]
        public string CarName { get; set; }

        [XmlAttribute]
        public string Simulator { get; set; }

        public Angle SteeringLock { get; set; }

        public ChartsProperties ChartsProperties { get; set; }

        public WheelPropertiesDto FrontLeftTyre { get; set; }

        public WheelPropertiesDto FrontRightTyre { get; set; }

        public WheelPropertiesDto RearLeftTyre { get; set; }

        public WheelPropertiesDto RearRightTyre { get; set; }

        public WheelPropertiesDto GetPropertiesByWheelKind(WheelKind wheelKind)
        {
            switch (wheelKind)
            {
                case WheelKind.FrontLeft:
                    return FrontLeftTyre;
                case WheelKind.FrontRight:
                    return FrontRightTyre;
                case WheelKind.RearLeft:
                    return RearLeftTyre;
                case WheelKind.RearRight:
                    return RearRightTyre;
                default:
                    throw new ArgumentOutOfRangeException(nameof(wheelKind), wheelKind, null);
            }
        }
    }
}