﻿namespace SecondMonitor.Telemetry.TelemetryApplication.Settings.DTO.CarProperties
{
    using DataModel.BasicProperties;

    public class WheelPropertiesDto
    {
        public WheelPropertiesDto()
        {
            BumpTransition = Velocity.FromMs(0.030);
            ReboundTransition = Velocity.FromMs(-0.030);
            IdealCamber = Angle.GetFromDegrees(0);
            NeutralSuspensionTravel = Distance.FromMeters(0);
            SprintStiffnessPerMm = Force.GetFromNewtons(150);
        }

        public Velocity BumpTransition { get; set; }

        public Velocity ReboundTransition { get; set; }

        public Angle IdealCamber { get; set; }

        public Force SprintStiffnessPerMm { get; set; }

        public Distance NeutralSuspensionTravel { get; set; }
    }
}