﻿namespace SecondMonitor.Telemetry.TelemetryApplication.ViewModels.GraphPanel.Chassis
{
    using System;
    using DataModel.BasicProperties;
    using DataModel.Snapshot;
    using DataModel.Snapshot.Systems;

    public class ChassisRideHeightGraphViewModel : AbstractChassisGraphViewModel
    {
        public override string Title => "Ride Height - Chassis";
        protected override string YUnits => Distance.GetUnitsSymbol(UnitsCollection.DistanceUnitsVerySmall);
        protected override double YTickInterval => Distance.FromMeters(0.02).GetByUnit(UnitsCollection.DistanceUnitsVerySmall);
        protected override Func<SimulatorSourceInfo, CarInfo, double> FrontFunc => (_, x) => x.FrontHeight?.GetByUnit(UnitsCollection.DistanceUnitsVerySmall) ?? 0;
        protected override Func<SimulatorSourceInfo, CarInfo, double> RearFunc => (_, x) => x.RearHeight?.GetByUnit(UnitsCollection.DistanceUnitsVerySmall) ?? 0;
    }
}