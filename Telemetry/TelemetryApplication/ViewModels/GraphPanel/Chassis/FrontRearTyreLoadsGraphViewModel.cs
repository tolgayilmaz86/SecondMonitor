﻿namespace SecondMonitor.Telemetry.TelemetryApplication.ViewModels.GraphPanel.Chassis
{
    using System;
    using System.Linq;
    using DataAdapters;
    using DataModel.BasicProperties;
    using DataModel.Snapshot;
    using DataModel.Snapshot.Systems;
    using TelemetryManagement.DTO;

    public class FrontRearTyreLoadsGraphViewModel : AbstractChassisGraphViewModel
    {
        private readonly TyreLoadAdapter _tyreLoadAdapter;
        private string _title = "Tyre Loads (F/R)";

        public FrontRearTyreLoadsGraphViewModel(TyreLoadAdapter tyreLoadAdapter)
        {
            _tyreLoadAdapter = tyreLoadAdapter;
        }
        public override string Title => _title;
        protected override string YUnits => Force.GetUnitSymbol(UnitsCollection.ForceUnits);
        protected override double YTickInterval => Force.GetFromNewtons(1000).GetValueInUnits(UnitsCollection.ForceUnits);
        protected override bool CanYZoom => true;
        public override bool IsCarSettingsDependant => true;

        protected override Func<SimulatorSourceInfo, CarInfo, double> FrontFunc => (sourceInfo, x) => _tyreLoadAdapter.GetQuantityFromWheel(sourceInfo, x.WheelsInfo.FrontLeft).GetValueInUnits(UnitsCollection.ForceUnits) +
                                                                                                      _tyreLoadAdapter.GetQuantityFromWheel(sourceInfo, x.WheelsInfo.FrontRight).GetValueInUnits(UnitsCollection.ForceUnits);
        protected override Func<SimulatorSourceInfo, CarInfo, double> RearFunc => (sourceInfo,x) => _tyreLoadAdapter.GetQuantityFromWheel(sourceInfo, x.WheelsInfo.RearLeft).GetValueInUnits(UnitsCollection.ForceUnits) +
                                                                                                    _tyreLoadAdapter.GetQuantityFromWheel(sourceInfo, x.WheelsInfo.RearRight).GetValueInUnits(UnitsCollection.ForceUnits);

        protected override void PreviewOnLapLoaded(LapTelemetryDto lapTelemetryDto)
        {
            var firstDataPoint = lapTelemetryDto.DataPoints.FirstOrDefault();
            string newTitle;
            if (firstDataPoint == null || !_tyreLoadAdapter.IsQuantityComputed(firstDataPoint.SimulatorSourceInfo))
            {
                newTitle = "Tyre Loads (F/R)";
            }
            else
            {
                newTitle = "Suspension Loads (F/R) (C)";
            }

            if (newTitle != _title)
            {
                _title = newTitle;
                NotifyPropertyChanged(nameof(Title));
            }
            base.PreviewOnLapLoaded(lapTelemetryDto);
        }
    }
}