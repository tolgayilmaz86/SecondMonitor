﻿namespace SecondMonitor.Telemetry.TelemetryApplication.ViewModels.GraphPanel
{
    using System;
    using System.Collections.Generic;
    using Controllers.Synchronization;
    using Controllers.Synchronization.Graphs;
    using DataModel.BasicProperties;
    using DataModel.BasicProperties.Units;
    using DataModel.Telemetry;
    using OxyPlot;
    using SecondMonitor.ViewModels;
    using Settings.DTO;
    using TelemetryManagement.DTO;

    public interface IGraphViewModel : IViewModel, IDisposable
    {
        string Title { get; }
        bool HasValidData { get; set; }
        PlotModel PlotModel { get; }
        XAxisKind XAxisKind { get; set; }
        ILapColorSynchronization LapColorSynchronization { get; set; }
        IGraphViewSynchronization GraphViewSynchronization { get; set; }

        bool IsCarSettingsDependant { get; }

        UnitsCollection UnitsCollection { get; set; }

        void AddLapTelemetry(LapTelemetryDto lapTelemetryDto);
        void AddLapTelemetries(IEnumerable<LapTelemetryDto> lapSummaryDtos);
        void RemoveLapTelemetry(LapSummaryDto lapSummaryDto);
        void UpdateXSelection(LapSummaryDto lapSummaryDto, TimedTelemetrySnapshot timedTelemetrySnapshot);
        void RemoveAllLapsTelemetry();
    }
}