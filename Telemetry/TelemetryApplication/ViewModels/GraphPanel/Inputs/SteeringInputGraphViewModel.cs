﻿namespace SecondMonitor.Telemetry.TelemetryApplication.ViewModels.GraphPanel.Inputs
{
    using System.Collections.Generic;
    using DataExtractor;
    using DataModel.Telemetry;

    public class SteeringInputGraphViewModel : AbstractSingleSeriesGraphViewModel
    {
        public SteeringInputGraphViewModel(IEnumerable<ISingleSeriesDataExtractor> dataExtractors) : base(dataExtractors)
        {
        }

        public override string Title => "Steering Input";
        protected override string YUnits => "%";
        protected override double YTickInterval => 25;
        protected override bool CanYZoom => true;
        /*protected override void UpdateYMaximum(LapTelemetryDto lapTelemetry)
        {
            double max = lapTelemetry.TimedTelemetrySnapshots.Max(x => x.InputInfo.WheelAngle);
            if (max > YMaximum)
            {
                YMinimum = -max;
                YMaximum = max;
            }
        }*/

        protected override double GetYValue(TimedTelemetrySnapshot value)
        {
            return value.InputInfo.SteeringInput * 100 ;
        }
    }
}