﻿namespace SecondMonitor.Telemetry.TelemetryManagement
{
    using System.Collections.Generic;
    using Contracts.NInject;
    using Ninject.Modules;

    public class TelemetryManagementModuleBootstrapper : INinjectModuleBootstrapper
    {
        public IList<INinjectModule> GetModules()
        {
            return new List<INinjectModule>(new NinjectModule []{ new TelemetryManagementModule()});
        }
    }
}